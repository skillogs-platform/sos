package sos

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestShouldPingServiceExistance(t *testing.T) {
	dto := ServiceDTO{Name: "name", Target: "https://service.skillogs.com", Matcher: "/base"}
	emiter := &testEmiter{}
	signal := New(emiter, 10*time.Millisecond, dto)

	go signal.Start()
	time.Sleep(100 * time.Millisecond)
	signal.Stop()
	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "name", dto.Name)
	assert.Equal(t, "/base", dto.Matcher)
	assert.Equal(t, "https://service.skillogs.com", dto.Target)
	assert.Condition(t, func() bool {
		return emiter.called >= 9 && emiter.called <= 11
	})
}

type testEmiter struct {
	called   int
	lastSent interface{}
}

func (te *testEmiter) Emit(dto interface{}) error {
	te.called++
	te.lastSent = dto
	return nil
}

func (te *testEmiter) Close() error { return nil }
