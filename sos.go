package sos

import (
	"io"
	"time"

	"github.com/sirupsen/logrus"
)

// ServiceDTO represent service configuration to be enalbed in gateway
type ServiceDTO struct {
	Name    string `json:"name"`
	Target  string `json:"target"`
	Matcher string `json:"matcher"`
}

// An Emitter can emit a ServiceDTO
type Emitter interface {
	Emit(interface{}) error
	io.Closer
}

// A Signal allow to emit a serviceDTO each duration
type Signal struct {
	emitter Emitter
	dtos    []ServiceDTO
	ticker  *time.Ticker
}

// New create a new Signal
func New(e Emitter, repeat time.Duration, dtos ...ServiceDTO) *Signal {
	return &Signal{
		emitter: e,
		dtos:    dtos,
		ticker:  time.NewTicker(repeat),
	}
}

// Start to emit serviceDTO
func (s Signal) Start() {
	t := s.ticker.C
	for {
		for _, dto := range s.dtos {
			if err := s.emitter.Emit(dto); err != nil {
				logrus.WithError(err).Errorf("Cannot emit service configuration")
			}
		}
		<-t
	}
}

// Stop stopped to emit serviceDTO
func (s Signal) Stop() {
	if err := s.emitter.Close(); err != nil {
		logrus.WithError(err).Errorf("Cannot close service emitter")
	}
	s.ticker.Stop()
}
