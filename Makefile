# export GOPATH=$(pwd) this is a library project, so we use default gopath because of project structure

gopath:
	@echo "GOPATH= ${GOPATH}"

install: gopath
	go get github.com/sirupsen/logrus
	go get github.com/streadway/amqp
	go get github.com/stretchr/testify/assert

test: gopath
	@go test ./...

cover:
	@go test -cover ./...

cover-out:
	@go test ./... -v -coverprofile .testCoverage.txt
	@go tool cover -func .testCoverage.txt
	@rm .testCoverage.txt

ci-lint:
	@go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
	${GOPATH}/bin/golangci-lint --disable=dupl --out-format=code-climate --issues-exit-code=0 --deadline=5m0s run ./... --enable-all

lint:
	@./bin/golangci-lint --disable=dupl --out-format=tab --issues-exit-code=0 --deadline=5m0s run ./... --enable-all

.PHONY: gopath install test cover-out lint ci-lint
